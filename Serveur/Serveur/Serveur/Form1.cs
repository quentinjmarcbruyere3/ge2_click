﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql.Data.MySqlClient;
using System.Net.Sockets;
using System.Threading;
using System.Net;

namespace Serveur
{
    public partial class Form1 : Form
    {
        //Declaration de la comunnication mysql
        private MySqlConnection connection;
        //Variable des informations pour mysql
        string connectionString = "SERVER=mysql.com; DATABASE=ge2click; UID=ge2click; PASSWORD=ge2click; convert zero datetime=True";
        //Variable pour l'adresse ip du serveur
        static IPAddress localAddr = IPAddress.Parse("127.0.0.1");
        //Declaration de la communication tcp
        static TcpListener tcpListener = new TcpListener(localAddr, 10);
        //Definie le nombre de connexion simultanées
        int nombreDeClient = 2;
        public Form1()
        {
            InitializeComponent();
        }

        public void button1_Click(object sender, EventArgs e)
        {
            //Declaration de la variable pour Mysql
            connection = new MySqlConnection(connectionString);
            
            //Mysql
            try
            {
                //Ouverture de la connexion Mysql
                connection.Open();
                //Ecrit Mysql connexion successfull dans le logiciel
                richTextBox1.AppendText("Mysql connexion successfull\r\n");
                //Définition de cmd pour les commandes mysql
                MySqlCommand cmd = connection.CreateCommand();
                //cmd prend la commande SHOW TABLES pour récupérer les tables sur le serveur mysql
                cmd.CommandText = "SHOW TABLES";
                //rdr prend le retour de la commande SHOW TABLES
                MySqlDataReader rdr = cmd.ExecuteReader();
                //Affiche les tables dans le logiciel
                richTextBox1.AppendText("List of tables :\r\n");
                //Lecture de rdr
                while(rdr.Read())
                {
                    //Affiche le contenue de rdr dans le logiciel
                    richTextBox1.AppendText(rdr.GetString(0)+"\r\n");
                }
                //Ferme la connexion
                rdr.Close();
            }
            //Gestion de l'erreur si impossible de se connecter à Mysql
            catch
            {
                //Affiche Erreur de connection Mysql
                richTextBox1.AppendText("Erreur de connection mysql\r\n");
            }

            //TCP
            try
            {
                //Lance la connection tcp
                tcpListener.Start();
                //Ecrit Serveur Tcp lancé avec le nombre de connexion 
                richTextBox1.AppendText("Serveur Tcp lance " + nombreDeClient + " connexion\r\n");
                //Boucle avec le nombre de client définie
                for (int i = 0; i < nombreDeClient; i++)
                {
                    //Créer un nouveau thread par client de la fonction Listeners
                    Thread newThread = new Thread(new ThreadStart(Listeners));
                    //Lance le thread créer
                    newThread.Start();
                }
            }
            //Gestion de l'erreur éventuel sur tcp
            catch
            {
                //Affiche erreur serveur tcp
                richTextBox1.AppendText("Erreur serveur Tcp\r\n");
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Fermeture Mysql
            try
            {
                //Ferme la connexion Mysql
                connection.Close();
                //Affiche Connexion Mysql fermé
                richTextBox1.AppendText("Connexion Mysql ferme\r\n");
            }
            //Gestion d'erreur si impossible de fermer la connexion Mysql
            catch
            {
                //Affiche Impossible de fermer la connexion Mysql
                richTextBox1.AppendText("Impossible de fermer la connexion Mysql\r\n");
            }

            //Fermeture thread
            try
            {
                //Ferme l'application
                Application.Exit();
                //Ferme les thread
                Environment.Exit(0);
            }
            //Gestion de l'erreur possible de la fermeture possible du logiciel
            catch
            {
                //Affiche impossible de fermer l'application
                richTextBox1.AppendText("Impossible de fermer le logiciel");
            }
        }


        public void Listeners()
        {
            //Boucle infinie pour que le serveur ne se ferme pas
            while(true)
            {
                //Attente d'un nouveau client
                Socket socketForClient = tcpListener.AcceptSocket();
                //Si un client se connect
                if (socketForClient.Connected)
                {
                    //Affiche qu'un client est connecté avec un invoke car nous somme dans un thread
                    richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Client:" + socketForClient.RemoteEndPoint + " est connecte au serveur\r\n"); }));
                    //Création de networkStream pour le client
                    NetworkStream networkStream = new NetworkStream(socketForClient);
                    //Création de streamWriter pour l'écriture au client
                    System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);
                    //Création de streamReader pour la lecture du client
                    System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
                    //Lecture du message client que l'on stocke dans une variable theString
                    string theString = streamReader.ReadLine();
                    //Ecrit le message reçu
                    richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + theString + "\r\n"); }));

                    //Condition pour les différentes demande lors de la connexion
                    switch(theString)
                    {
                        //Si le message est inscription
                        case "inscription":
                            //On récupère l'email depuis le client
                            string inscriptionEmail = streamReader.ReadLine();
                            //On affiche l'email dans le debugeur
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + inscriptionEmail + "\r\n"); }));
                            //On récupère le mdp depuis le client
                            string inscriptionMdp = streamReader.ReadLine();
                            //On affiche le mdp dans le debugeur
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + inscriptionMdp + "\r\n"); }));

                            //On initialise la commande pour mysql
                            MySqlCommand inscriptionCmd = connection.CreateCommand();
                            //cmd prend une commande pour récupérer l'email si il existe sur la table sur le serveur mysql
                            inscriptionCmd.CommandText = "SELECT email FROM User WHERE email = '" + inscriptionEmail + "'";
                            //Prend le retour de la commande
                            MySqlDataReader inscriptionRdr = inscriptionCmd.ExecuteReader();
                            //Créer une variable pour stocker le retour
                            string emailRepInscription = "";
                            //Tant qu'il y a des choses à lire
                            while(inscriptionRdr.Read())
                            {
                                //Lecture du retour
                                emailRepInscription = inscriptionRdr.GetString(0);
                                //Affiche le retour dans le debeuger
                                richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message SQL: " + emailRepInscription + "\r\n"); }));
                            }
                            //Ferme la requète SQL
                            inscriptionRdr.Close();
                            //Si l'email n'existe pas
                            if (emailRepInscription == "")
                            {
                                //Commande pour enregistrer l'email et le mdp dans la base de donnée
                                inscriptionCmd.CommandText = "INSERT INTO User (email, mdp) VALUES (@email, @mdp)";
                                //Inclue la variable email
                                inscriptionCmd.Parameters.AddWithValue("@email", inscriptionEmail);
                                //Inclue la variable mdp
                                inscriptionCmd.Parameters.AddWithValue("@mdp", inscriptionMdp);
                                //Exécute la commande sans retour
                                inscriptionCmd.ExecuteNonQuery();

                                //Prépare le message 
                                streamWriter.WriteLine("ok");
                                //Envoie le message
                                streamWriter.Flush();
                            }
                            //Sinon
                            else
                            {
                                //Prépare le message
                                streamWriter.WriteLine("non");
                                //Envoie le message
                                streamWriter.Flush();
                            }
                            //Fin des choix
                            inscriptionRdr.Close();
                            //Fin de la condition
                            break;

                        //Si le message est connexion
                        case "connexion":
                            //Récupère l'email
                            string connexionEmail = streamReader.ReadLine();
                            //Affiche l'email dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + connexionEmail + "\r\n"); }));
                            //Récupère le mdp
                            string connexionMdp = streamReader.ReadLine();
                            //Affiche le mdp dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + connexionMdp + "\r\n"); }));


                            //Créer la commande pour mysql
                            MySqlCommand connexionCmd = connection.CreateCommand();
                            //cmd prend la commande pour récupérer des infos si l'email et le mdp sont les bons sur le serveur mysql
                            connexionCmd.CommandText = "SELECT nom, prenom FROM User WHERE email = '" + connexionEmail + "' AND mdp = '" + connexionMdp + "'";
                            //Prend le retour de la commande
                            MySqlDataReader connexionRdr = connexionCmd.ExecuteReader();
                            //Créer une variable pour le retour
                            string emailRepConnexion = "";
                            //Créer une variable pour stocker le prénom
                            string prenom4 = "";
                            //Tant qu'il y a des choses à lire
                            while (connexionRdr.Read())
                            {
                                //Lecture du retour
                                emailRepConnexion = connexionRdr.GetString(0);
                                //Affiche le retour dans le debug
                                richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message SQL: " + emailRepConnexion + "\r\n"); }));
                                //Lecture du retour
                                prenom4 = connexionRdr.GetString(1);
                                //Affiche le retour dans le debug
                                richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message SQL: " + prenom4 + "\r\n"); }));
                            }
                            //Ferme la requête
                            connexionRdr.Close();
                            //Si l'email et mdp n'existe pas
                            if (emailRepConnexion == "")
                            {
                                //Prépare le message
                                streamWriter.WriteLine("non");
                                //Envoie le message non
                                streamWriter.Flush();
                            }
                            //Sinon
                            else
                            {
                                //Prépare le message
                                streamWriter.WriteLine("ok");
                                //Envoie le message
                                streamWriter.Flush();
                                //Prépare le message
                                streamWriter.WriteLine(emailRepConnexion);
                                //Envoie le message
                                streamWriter.Flush();
                                //Prépare le message
                                streamWriter.WriteLine(prenom4);
                                //Envoie le message
                                streamWriter.Flush();
                            }
                            //Ferme la requête
                            connexionRdr.Close();
                            //Fin de cette condition
                            break;

                        //Si le message est publication
                        case "publication":
                            //Récupère l'email
                            string email = streamReader.ReadLine();
                            //Affiche l'email dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + email + "\r\n"); }));

                            //Récupère le texte
                            string texte = streamReader.ReadLine();
                            //Affiche le texte dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + texte + "\r\n"); }));

                            //Initialise la commande
                            MySqlCommand emailCmd = connection.CreateCommand();
                            //cmd prend la commande pour récupérer des infos si l'email et le mdp sont les bons sur le serveur mysql
                            emailCmd.CommandText = "SELECT id FROM User WHERE email = '" + email + "'";
                            //Prend le retour de la commande
                            MySqlDataReader emailRdr = emailCmd.ExecuteReader();
                            //Variable pour stocker id
                            string idRep = "";
                            //Tant qu'il y a des choses à lire
                            while (emailRdr.Read())
                            {
                                //Lecture du retour
                                idRep = emailRdr.GetString(0);
                                //Affiche le retour dans le debug
                                richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message SQL: " + idRep + "\r\n"); }));
                            }
                            //Ferme la requête
                            emailRdr.Close();

                            //On initialise la commande pour mysql
                            MySqlCommand postCmd = connection.CreateCommand();
                            //Commande insert pour enregistrer une publication
                            postCmd.CommandText = "INSERT INTO Post (utilisateur, post, date) VALUES (@utilisateur, @post, @date)";
                            //Inclue la variable email
                            postCmd.Parameters.AddWithValue("@utilisateur", idRep);
                            //Inclue la variable texte
                            postCmd.Parameters.AddWithValue("@post", texte);
                            //Inclue la variable date
                            postCmd.Parameters.AddWithValue("@date", DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss"));
                            //Exécute la commande sans retour
                            postCmd.ExecuteNonQuery();

                            //Prépare le message
                            streamWriter.WriteLine("ok");
                            //Envoie le message
                            streamWriter.Flush();
                            //Ferme la requête
                            emailRdr.Close();
                            //Fin de cette condition
                            break;

                        //Si le message est reception
                        case "reception":
                            //Initialise la commande
                            MySqlCommand recptionPublication = connection.CreateCommand();
                            //Le cmd prend la commande pour récupérer des infos si l'email et le mdp sont les bons sur le serveur mysql
                            recptionPublication.CommandText = "SELECT Post.id, CONCAT(User.prenom, ' ', User.nom) AS nom, Post.date, Post.post FROM Post INNER JOIN User ON Post.utilisateur = User.id ORDER BY Post.id DESC LIMIT 10";
                            //Prend le retour de la commande
                            MySqlDataReader receptionRdr = recptionPublication.ExecuteReader();
                            //Variable pour stocker id
                            string receptionRep = "";
                            //Tant qu'il y a des choses à lire
                            while (receptionRdr.Read())
                            {
                                //Boucle for afin de séparer les 4 éléments
                                for (int i = 0; i < 4; i++)
                                {
                                    //Lecture du retour
                                    receptionRep = receptionRdr.GetString(i);
                                    //Affiche le retour dans le debug
                                    richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message SQL: " + receptionRep + "\r\n"); }));
                                    //Prépare le message
                                    streamWriter.WriteLine(receptionRep);
                                    //Envoie le message
                                    streamWriter.Flush();
                                }
                            }

                            //Prépare le message
                            streamWriter.WriteLine("fin");
                            //Envoie le message
                            streamWriter.Flush();
                            //Ferme la requête
                            receptionRdr.Close();
                            //Fin de la condition
                            break;

                        //Si le message est updateprofile
                        case "updateprofile":
                            //Récupère l'email
                            string email1 = streamReader.ReadLine();
                            //Affiche l'email dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + email1 + "\r\n"); }));

                            //Récupère le nom
                            string nom = streamReader.ReadLine();
                            //Affiche le texte dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + nom + "\r\n"); }));

                            //Récupère le prenom
                            string prenom = streamReader.ReadLine();
                            //Affiche le texte dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + prenom + "\r\n"); }));

                            //Récupère la matiere forte
                            string matiereforte = streamReader.ReadLine();
                            //Affiche le texte dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + matiereforte + "\r\n"); }));

                            //Récupère la matiere forte 2
                            string matiereforte1 = streamReader.ReadLine();
                            //Affiche le texte dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + matiereforte1 + "\r\n"); }));

                            //Récupère la matiere faible
                            string matierefaible = streamReader.ReadLine();
                            //Affiche le texte dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + matierefaible + "\r\n"); }));

                            //Récupère la matiere faible 2
                            string matierefaible1 = streamReader.ReadLine();
                            //Affiche le texte dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + matierefaible1 + "\r\n"); }));

                            //On initialise la commande pour mysql
                            MySqlCommand profileupdateCmd = connection.CreateCommand();
                            //Commande update avec tous les attributs du profil
                            profileupdateCmd.CommandText = "UPDATE User SET nom = @nom, prenom = @prenom, matiere_forte = @matiere_forte, matiere_faible = @matiere_faible, matiere_forte1 = @matiere_forte1, matiere_faible1 = @matiere_faible1 WHERE email = @email";
                            //Inclue la variable nom
                            profileupdateCmd.Parameters.AddWithValue("@nom", nom);
                            //Inclue la variable prenom
                            profileupdateCmd.Parameters.AddWithValue("@prenom", prenom);
                            //Inclue la variable matiere forte
                            profileupdateCmd.Parameters.AddWithValue("@matiere_forte", matiereforte);
                            //Inclue la variable matiere faible
                            profileupdateCmd.Parameters.AddWithValue("@matiere_faible", matierefaible);
                            //Inclue la variable matiere forte 2
                            profileupdateCmd.Parameters.AddWithValue("@matiere_forte1", matiereforte1);
                            //Inclue la variable matiere faible 2
                            profileupdateCmd.Parameters.AddWithValue("@matiere_faible1", matierefaible1);
                            //Inclue la variable email
                            profileupdateCmd.Parameters.AddWithValue("@email", email1);
                            //Exécute la commande sans retour
                            profileupdateCmd.ExecuteNonQuery();
                            //Prépare le message
                            streamWriter.WriteLine("ok");
                            //Envoie le message
                            streamWriter.Flush();
                            //Fin de la condition
                            break;

                        //Si le message est receptprofile
                        case "receptprofile":
                            //Récupère l'email
                            string email2 = streamReader.ReadLine();
                            //Affiche l'email dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + email2 + "\r\n"); }));

                            //Initialise la commande
                            MySqlCommand receptprofileCmd = connection.CreateCommand();
                            //Commande select sur tous les attributs du profil
                            receptprofileCmd.CommandText = "SELECT nom, prenom, matiere_forte, matiere_forte1, matiere_faible, matiere_faible1 FROM User WHERE email = '" + email2 + "'";
                            //Prend le retour de la commande
                            MySqlDataReader receptprofileRdr = receptprofileCmd.ExecuteReader();
                            //Variable pour stocker la réponse
                            string Rep = "";
                            //Tant qu'il y a des choses à lire
                            while (receptprofileRdr.Read())
                            {
                                //Boucle for pour séparer les éléments
                                for (int i = 0; i < 6; i++)
                                {
                                    //Si bon retour
                                    try
                                    {
                                        //Lecture du retour
                                        Rep = receptprofileRdr.GetString(i);
                                    }
                                    //Sinon gestion de l'erreur
                                    catch
                                    {
                                        Rep = " ";
                                    }
                                    //Affiche le retour dans le debug
                                    richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message SQL: " + Rep + "\r\n"); }));
                                    //Prépare le message
                                    streamWriter.WriteLine(Rep);
                                    //Envoie le message
                                    streamWriter.Flush();
                                }
                            }
                            //Ferme la requête
                            receptprofileRdr.Close();
                            //Prépare le message
                            streamWriter.WriteLine("ok");
                            //Envoie le message
                            streamWriter.Flush();
                            //Fin de la condition
                            break;

                        //Si le message est inscriptionbis
                        case "inscriptionbis":
                            //Récupère l'email
                            string email3 = streamReader.ReadLine();
                            //Affiche l'email dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + email3 + "\r\n"); }));

                            //Récupère le nom
                            string nom1 = streamReader.ReadLine();
                            //Affiche le texte dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + nom1 + "\r\n"); }));

                            //Récupère le prenom
                            string prenom1 = streamReader.ReadLine();
                            //Affiche le texte dans le debug
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message recu: " + prenom1 + "\r\n"); }));

                            //On initialise la commande pour mysql
                            MySqlCommand profilecreateCmd = connection.CreateCommand();
                            //Commande update sur nom et prenom
                            profilecreateCmd.CommandText = "UPDATE User SET nom = @nom, prenom = @prenom WHERE email = @email";
                            //Inclue la variable nom
                            profilecreateCmd.Parameters.AddWithValue("@nom", nom1);
                            //Inclue la variable prenom
                            profilecreateCmd.Parameters.AddWithValue("@prenom", prenom1);
                            //Inclut la variable email
                            profilecreateCmd.Parameters.AddWithValue("@email", email3);
                            //Exécute la commande sans retour
                            profilecreateCmd.ExecuteNonQuery();
                            //Prépare le message
                            streamWriter.WriteLine("ok");
                            //Envoie le message
                            streamWriter.Flush();
                            //Fin de la condition
                            break;

                        //Si le message est recherche
                        case "recherche":
                            //Récupère la recherche
                            string recherche = streamReader.ReadLine();
                            //Initialise la commande
                            MySqlCommand recptionPublicationrecherche = connection.CreateCommand();
                            //Commande pour récupérer les messages correspondants
                            recptionPublicationrecherche.CommandText = "SELECT Post.id, CONCAT(User.prenom, ' ', User.nom) AS nom, Post.date, Post.post FROM Post INNER JOIN User ON Post.utilisateur = User.id WHERE Post.post like '%" + recherche + "%' OR User.prenom like '%" + recherche + "%' OR User.nom like '%" + recherche + "%' ORDER BY Post.id DESC LIMIT 10";
                            //Prend le retour de la commande
                            MySqlDataReader receptionrechercheRdr = recptionPublicationrecherche.ExecuteReader();
                            //Variable pour stocker la réponse
                            string receptionrechercheRep = "";
                            //Tant qu'il y a des choses à lire
                            while (receptionrechercheRdr.Read())
                            {
                                //Boucle for pour séparer chaque éléments
                                for (int i = 0; i < 4; i++)
                                {
                                    //Lecture du retour
                                    receptionrechercheRep = receptionrechercheRdr.GetString(i);
                                    //Affiche le retour dans le debug
                                    richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Message SQL: " + receptionrechercheRep + "\r\n"); }));
                                    //Prépare le message
                                    streamWriter.WriteLine(receptionrechercheRep);
                                    //Envoie le message
                                    streamWriter.Flush();
                                }
                            }

                            //Prépare le message
                            streamWriter.WriteLine("fin");
                            //Envoie le message
                            streamWriter.Flush();
                            //Ferme la requête
                            receptionrechercheRdr.Close();
                            //Fin de la condition
                            break;
                    }

                    //Fermeture de streamReader
                    streamReader.Close();
                    //Fermeture de la connexion
                    networkStream.Close();
                    //Fermeture de streamWriter
                    streamWriter.Close();

                }
                //Fermeture de la connexion client
                socketForClient.Close();
                //Ecrit que la connexion est ferme
                richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("Connexion ferme\r\n"); }));
            }
            
        }
    }
}
