# GE2_Click

GE2_Click is a social network made by the GEII for the GEII.
This project was create for our last of GEII.

## Local setup

The project contain one folder for the server part ans one folder for the client part.
For connect it you have just to start the server and when you start the client the connection is automatic.
You need to create a database and modify the line for the connection in the server code.

## Code

This project was completely create with visual studio and code in c#.

## Test

The folder named test contains the projects that have been used for the test part of the project.
