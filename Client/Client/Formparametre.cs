﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;

namespace Client
{
    public partial class Formparametre : Form
    {
        //Variable global email
        string email = Form1.email;
        public Formparametre()
        {
            InitializeComponent();
        }

        private void buttonquitter_Click(object sender, EventArgs e)
        {
            //Ferme la fenêtre
            this.Close();
        }

        private void buttonsauvegarder_Click(object sender, EventArgs e)
        {
            //Création d'un nouveau thread
            Thread newThread = new Thread(new ThreadStart(UpdateProfile));
            //Lance le thread
            newThread.Start();
        }

        private void Formparametre_Load(object sender, EventArgs e)
        {
            //Création d'un nouveau thread
            Thread newThread = new Thread(new ThreadStart(ReceptProfile));
            //Lance le thread
            newThread.Start();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        public void UpdateProfile()
        {
            //Gestion des erreurs de communication
            try
            {
                //Création de socketForServer de tcp client
                TcpClient socketForServer;
                //Définie la connexion sur localhost, sur le port 10
                socketForServer = new TcpClient("localhost", 10);
                //Créer networkStream
                NetworkStream networkStream = socketForServer.GetStream();
                //Créer streamReader pour lire le serveur
                System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
                //Créer streamWriter pour écrire au client
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);

                //Créer une variable str pour envoyer le message
                string str = "updateprofile";
                //Prépare le message à envoyer
                streamWriter.WriteLine(str);
                //Envoie le message
                streamWriter.Flush();

                //Prépare le message à envoyer
                streamWriter.WriteLine(email);
                //Envoie le message
                streamWriter.Flush();

                //Créer une variable publication pour envoyer le message
                string nom = "";
                //Récupère le texte dans la textBox
                textBoxnom.Invoke(new MethodInvoker(delegate { nom = textBoxnom.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(nom);
                //Envoie le message
                streamWriter.Flush();

                //Créer une variable pour envoyer le message
                string prenom = "";
                //Récupère le texte dans la textBox
                textBoxprenom.Invoke(new MethodInvoker(delegate { prenom = textBoxprenom.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(prenom);
                //Envoie le message
                streamWriter.Flush();

                //Créer une variable pour envoyer le message
                string matiereforte = "";
                //Récupère le texte dans la textBox
                comboBoxmatieref1.Invoke(new MethodInvoker(delegate { matiereforte = comboBoxmatieref1.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(matiereforte);
                //Envoie le message
                streamWriter.Flush();

                //Créer une variable pour envoyer le message
                string matiereforte1 = "";
                //Récupère le texte dans la textBox
                comboBoxmatieref2.Invoke(new MethodInvoker(delegate { matiereforte1 = comboBoxmatieref2.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(matiereforte1);
                //Envoie le message
                streamWriter.Flush();

                //Créer une variable pour envoyer le message
                string matierefaible = "";
                //Récupère le texte dans la textBox
                comboBoxmatierefa1.Invoke(new MethodInvoker(delegate { matierefaible = comboBoxmatierefa1.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(matierefaible);
                //Envoie le message
                streamWriter.Flush();

                //Créer une variable pour envoyer le message
                string matierefaible1 = "";
                //Récupère le texte dans la textBox
                comboBoxmatierefa2.Invoke(new MethodInvoker(delegate { matierefaible1 = comboBoxmatierefa2.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(matierefaible1);
                //Envoie le message
                streamWriter.Flush();

                //Lecture du message envoyé par le serveur
                string con = streamReader.ReadLine();

                //Si la réponse est ok
                if (con == "ok")
                {
                    //Affiche un message
                    erreur.Invoke(new MethodInvoker(delegate { erreur.Text = "Profile modifié"; }));
                    //Rend le message visible
                    erreur.Invoke(new MethodInvoker(delegate { erreur.Visible = true; }));
                    //Affiche le message en vert
                    erreur.Invoke(new MethodInvoker(delegate { erreur.ForeColor = Color.Green; }));
                    //Ferme la fenêtre
                    this.Invoke(new MethodInvoker(delegate { this.Close(); }));
                }
                //Sinon
                else
                {
                    //Affiche un message d'erreur
                    erreur.Invoke(new MethodInvoker(delegate { erreur.Text = "Impossible de modifier le profil"; }));
                    //Rend le message visible
                    erreur.Invoke(new MethodInvoker(delegate { erreur.Visible = true; }));
                    //Affiche le message en rouge
                    erreur.Invoke(new MethodInvoker(delegate { erreur.ForeColor = Color.Red; }));
                }
                //Ferme la connexion
                networkStream.Close();
            }
            //Si une erreur de communication arrive affiche un message d'erreur
            catch
            {

                //Affiche un message d'erreur
                erreur.Invoke(new MethodInvoker(delegate { erreur.Text = "Communication impossible"; }));
                //Rend le message visible
                erreur.Invoke(new MethodInvoker(delegate { erreur.Visible = true; }));
                //Affiche le message en rouge
                erreur.Invoke(new MethodInvoker(delegate { erreur.ForeColor = Color.Red; }));

            }
        }

        public void ReceptProfile()
        {
            //Gestion des erreurs de communication
            try
            {
                //Création de socketForServer de tcp client
                TcpClient socketForServer;
                //Définie la connexion sur localhost, sur le port 10
                socketForServer = new TcpClient("localhost", 10);
                //Créer networkStream
                NetworkStream networkStream = socketForServer.GetStream();
                //Créer streamReader pour lire le serveur
                System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
                //Créer streamWriter pour écrire au client
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);

                //Créer une variable str pour envoyer le message
                string str = "receptprofile";
                //Prépare le message à envoyer
                streamWriter.WriteLine(str);
                //Envoie le message
                streamWriter.Flush();

                //Prépare le message à envoyer
                streamWriter.WriteLine(email);
                //Envoie le message
                streamWriter.Flush();

                //Lecture du message nom
                string nom = streamReader.ReadLine();
                //Lecture du message prenom
                string prenom = streamReader.ReadLine();
                //Lecture du message matiere forte
                string matiere_forte = streamReader.ReadLine();
                //Lecture du message matiere forte 2
                string matiere_forte1 = streamReader.ReadLine();
                //Lecture du message matiere faible
                string matiere_faible = streamReader.ReadLine();
                //Lecture du message matiere faible 2
                string matiere_faible1 = streamReader.ReadLine();

                //Lecture du message envoyé par le serveur
                string con = streamReader.ReadLine();

                //Si message ok
                if (con == "ok")
                {
                    //Affiche un message
                    erreur.Invoke(new MethodInvoker(delegate { erreur.Text = "Profile chargé"; }));
                    //Rend le message visible
                    erreur.Invoke(new MethodInvoker(delegate { erreur.Visible = true; }));
                    //Affiche le message en vert
                    erreur.Invoke(new MethodInvoker(delegate { erreur.ForeColor = Color.Green; }));
                    //Affiche l'email du profil
                    labelprofil.Invoke(new MethodInvoker(delegate { labelprofil.Text = "Profil : " + email;  }));
                    //Affiche le nom dans la case nom
                    textBoxnom.Invoke(new MethodInvoker(delegate { textBoxnom.Text = nom; }));
                    //Affiche le prenom dans la case prenom
                    textBoxprenom.Invoke(new MethodInvoker(delegate { textBoxprenom.Text = prenom; }));
                    //Affiche la matiere forte dans la case matiere forte
                    comboBoxmatieref1.Invoke(new MethodInvoker(delegate { comboBoxmatieref1.Text = matiere_forte; }));
                    //Affiche la matiere forte 2 dans la case matiere forte 2
                    comboBoxmatieref2.Invoke(new MethodInvoker(delegate { comboBoxmatieref2.Text = matiere_forte1; }));
                    //Affiche la matiere faible dans la case matiere faible
                    comboBoxmatierefa1.Invoke(new MethodInvoker(delegate { comboBoxmatierefa1.Text = matiere_faible; }));
                    //Affiche la matiere faible 2 dans la case matiere faible 2
                    comboBoxmatierefa2.Invoke(new MethodInvoker(delegate { comboBoxmatierefa2.Text = matiere_faible1; }));
                }
                //Sinon
                else
                {
                    //Affiche un message d'erreur
                    erreur.Invoke(new MethodInvoker(delegate { erreur.Text = "Impossible de charger le profil"; }));
                    //Rend le message visible
                    erreur.Invoke(new MethodInvoker(delegate { erreur.Visible = true; }));
                    //Affiche le message en rouge
                    erreur.Invoke(new MethodInvoker(delegate { erreur.ForeColor = Color.Red; }));
                }
                //Ferme la connexion
                networkStream.Close();
            }
            //Si une erreur de connexion arrive affiche un message d'erreur
            catch
            {

                //Affiche un message d'erreur
                erreur.Invoke(new MethodInvoker(delegate { erreur.Text = "Communication impossible"; }));
                //Rend le message visible
                erreur.Invoke(new MethodInvoker(delegate { erreur.Visible = true; }));
                //Affiche le message en rouge
                erreur.Invoke(new MethodInvoker(delegate { erreur.ForeColor = Color.Red; }));

            }
        }

        private void textBoxnom_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
