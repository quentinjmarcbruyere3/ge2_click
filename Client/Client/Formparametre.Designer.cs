﻿namespace Client
{
    partial class Formparametre
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelprofil = new System.Windows.Forms.Label();
            this.buttonquitter = new System.Windows.Forms.Button();
            this.buttonsauvegarder = new System.Windows.Forms.Button();
            this.labelnom = new System.Windows.Forms.Label();
            this.textBoxnom = new System.Windows.Forms.TextBox();
            this.labelprenom = new System.Windows.Forms.Label();
            this.textBoxprenom = new System.Windows.Forms.TextBox();
            this.labelmatieref1 = new System.Windows.Forms.Label();
            this.labelmatierefa1 = new System.Windows.Forms.Label();
            this.comboBoxmatieref1 = new System.Windows.Forms.ComboBox();
            this.comboBoxmatierefa1 = new System.Windows.Forms.ComboBox();
            this.comboBoxmatieref2 = new System.Windows.Forms.ComboBox();
            this.labelmatieref2 = new System.Windows.Forms.Label();
            this.labelmatierefa2 = new System.Windows.Forms.Label();
            this.comboBoxmatierefa2 = new System.Windows.Forms.ComboBox();
            this.erreur = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelprofil
            // 
            this.labelprofil.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelprofil.AutoSize = true;
            this.labelprofil.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelprofil.Location = new System.Drawing.Point(14, 9);
            this.labelprofil.Name = "labelprofil";
            this.labelprofil.Size = new System.Drawing.Size(108, 36);
            this.labelprofil.TabIndex = 0;
            this.labelprofil.Text = "Profil :";
            this.labelprofil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // buttonquitter
            // 
            this.buttonquitter.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonquitter.Location = new System.Drawing.Point(410, 409);
            this.buttonquitter.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonquitter.Name = "buttonquitter";
            this.buttonquitter.Size = new System.Drawing.Size(149, 50);
            this.buttonquitter.TabIndex = 1;
            this.buttonquitter.Text = "Quitter";
            this.buttonquitter.UseVisualStyleBackColor = true;
            this.buttonquitter.Click += new System.EventHandler(this.buttonquitter_Click);
            // 
            // buttonsauvegarder
            // 
            this.buttonsauvegarder.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonsauvegarder.Location = new System.Drawing.Point(19, 409);
            this.buttonsauvegarder.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.buttonsauvegarder.Name = "buttonsauvegarder";
            this.buttonsauvegarder.Size = new System.Drawing.Size(149, 50);
            this.buttonsauvegarder.TabIndex = 2;
            this.buttonsauvegarder.Text = "Sauvegarder";
            this.buttonsauvegarder.UseVisualStyleBackColor = true;
            this.buttonsauvegarder.Click += new System.EventHandler(this.buttonsauvegarder_Click);
            // 
            // labelnom
            // 
            this.labelnom.AutoSize = true;
            this.labelnom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelnom.Location = new System.Drawing.Point(15, 63);
            this.labelnom.Name = "labelnom";
            this.labelnom.Size = new System.Drawing.Size(64, 25);
            this.labelnom.TabIndex = 3;
            this.labelnom.Text = "Nom :";
            this.labelnom.Click += new System.EventHandler(this.label1_Click);
            // 
            // textBoxnom
            // 
            this.textBoxnom.Location = new System.Drawing.Point(38, 105);
            this.textBoxnom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxnom.Name = "textBoxnom";
            this.textBoxnom.Size = new System.Drawing.Size(151, 22);
            this.textBoxnom.TabIndex = 4;
            this.textBoxnom.Text = "\r\n";
            this.textBoxnom.TextChanged += new System.EventHandler(this.textBoxnom_TextChanged);
            // 
            // labelprenom
            // 
            this.labelprenom.AutoSize = true;
            this.labelprenom.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelprenom.Location = new System.Drawing.Point(377, 63);
            this.labelprenom.Name = "labelprenom";
            this.labelprenom.Size = new System.Drawing.Size(91, 25);
            this.labelprenom.TabIndex = 5;
            this.labelprenom.Text = "Prénom :";
            // 
            // textBoxprenom
            // 
            this.textBoxprenom.Location = new System.Drawing.Point(408, 105);
            this.textBoxprenom.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.textBoxprenom.Name = "textBoxprenom";
            this.textBoxprenom.Size = new System.Drawing.Size(151, 22);
            this.textBoxprenom.TabIndex = 6;
            // 
            // labelmatieref1
            // 
            this.labelmatieref1.AutoSize = true;
            this.labelmatieref1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelmatieref1.Location = new System.Drawing.Point(15, 190);
            this.labelmatieref1.Name = "labelmatieref1";
            this.labelmatieref1.Size = new System.Drawing.Size(174, 25);
            this.labelmatieref1.TabIndex = 7;
            this.labelmatieref1.Text = "1ère matière forte :";
            // 
            // labelmatierefa1
            // 
            this.labelmatierefa1.AutoSize = true;
            this.labelmatierefa1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelmatierefa1.Location = new System.Drawing.Point(377, 190);
            this.labelmatierefa1.Name = "labelmatierefa1";
            this.labelmatierefa1.Size = new System.Drawing.Size(182, 25);
            this.labelmatierefa1.TabIndex = 8;
            this.labelmatierefa1.Text = "1ère matière faible :";
            // 
            // comboBoxmatieref1
            // 
            this.comboBoxmatieref1.FormattingEnabled = true;
            this.comboBoxmatieref1.Items.AddRange(new object[] {
            "Anglais",
            "Automatique",
            "Automatismes",
            "Energie",
            "Info",
            "Maths",
            "OL",
            "Physique",
            "Réseaux",
            "SE",
            "SIN"});
            this.comboBoxmatieref1.Location = new System.Drawing.Point(38, 227);
            this.comboBoxmatieref1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxmatieref1.Name = "comboBoxmatieref1";
            this.comboBoxmatieref1.Size = new System.Drawing.Size(151, 24);
            this.comboBoxmatieref1.TabIndex = 9;
            // 
            // comboBoxmatierefa1
            // 
            this.comboBoxmatierefa1.FormattingEnabled = true;
            this.comboBoxmatierefa1.Items.AddRange(new object[] {
            "Anglais",
            "Automatique",
            "Automatismes",
            "Energie",
            "Info",
            "Maths",
            "OL",
            "Physique",
            "Réseaux",
            "SE",
            "SIN"});
            this.comboBoxmatierefa1.Location = new System.Drawing.Point(408, 227);
            this.comboBoxmatierefa1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxmatierefa1.Name = "comboBoxmatierefa1";
            this.comboBoxmatierefa1.Size = new System.Drawing.Size(151, 24);
            this.comboBoxmatierefa1.TabIndex = 10;
            // 
            // comboBoxmatieref2
            // 
            this.comboBoxmatieref2.FormattingEnabled = true;
            this.comboBoxmatieref2.Items.AddRange(new object[] {
            "Anglais",
            "Automatique",
            "Automatismes",
            "Energie",
            "Info",
            "Maths",
            "OL",
            "Physique",
            "Réseaux",
            "SE",
            "SIN"});
            this.comboBoxmatieref2.Location = new System.Drawing.Point(38, 321);
            this.comboBoxmatieref2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxmatieref2.Name = "comboBoxmatieref2";
            this.comboBoxmatieref2.Size = new System.Drawing.Size(151, 24);
            this.comboBoxmatieref2.TabIndex = 11;
            // 
            // labelmatieref2
            // 
            this.labelmatieref2.AutoSize = true;
            this.labelmatieref2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelmatieref2.Location = new System.Drawing.Point(15, 282);
            this.labelmatieref2.Name = "labelmatieref2";
            this.labelmatieref2.Size = new System.Drawing.Size(184, 25);
            this.labelmatieref2.TabIndex = 12;
            this.labelmatieref2.Text = "2ème matière forte :";
            // 
            // labelmatierefa2
            // 
            this.labelmatierefa2.AutoSize = true;
            this.labelmatierefa2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelmatierefa2.Location = new System.Drawing.Point(377, 282);
            this.labelmatierefa2.Name = "labelmatierefa2";
            this.labelmatierefa2.Size = new System.Drawing.Size(192, 25);
            this.labelmatierefa2.TabIndex = 13;
            this.labelmatierefa2.Text = "2ème matière faible :";
            // 
            // comboBoxmatierefa2
            // 
            this.comboBoxmatierefa2.FormattingEnabled = true;
            this.comboBoxmatierefa2.Items.AddRange(new object[] {
            "Anglais",
            "Automatique",
            "Automatismes",
            "Energie",
            "Info",
            "Maths",
            "OL",
            "Physique",
            "Réseaux",
            "SE",
            "SIN"});
            this.comboBoxmatierefa2.Location = new System.Drawing.Point(410, 321);
            this.comboBoxmatierefa2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.comboBoxmatierefa2.Name = "comboBoxmatierefa2";
            this.comboBoxmatierefa2.Size = new System.Drawing.Size(149, 24);
            this.comboBoxmatierefa2.TabIndex = 14;
            // 
            // erreur
            // 
            this.erreur.AutoSize = true;
            this.erreur.Location = new System.Drawing.Point(268, 48);
            this.erreur.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.erreur.Name = "erreur";
            this.erreur.Size = new System.Drawing.Size(46, 17);
            this.erreur.TabIndex = 15;
            this.erreur.Text = "label1";
            this.erreur.Visible = false;
            // 
            // Formparametre
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(582, 503);
            this.Controls.Add(this.erreur);
            this.Controls.Add(this.comboBoxmatierefa2);
            this.Controls.Add(this.labelmatierefa2);
            this.Controls.Add(this.labelmatieref2);
            this.Controls.Add(this.comboBoxmatieref2);
            this.Controls.Add(this.comboBoxmatierefa1);
            this.Controls.Add(this.comboBoxmatieref1);
            this.Controls.Add(this.labelmatierefa1);
            this.Controls.Add(this.labelmatieref1);
            this.Controls.Add(this.textBoxprenom);
            this.Controls.Add(this.labelprenom);
            this.Controls.Add(this.textBoxnom);
            this.Controls.Add(this.labelnom);
            this.Controls.Add(this.buttonsauvegarder);
            this.Controls.Add(this.buttonquitter);
            this.Controls.Add(this.labelprofil);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximumSize = new System.Drawing.Size(600, 550);
            this.MinimumSize = new System.Drawing.Size(600, 550);
            this.Name = "Formparametre";
            this.Text = "Paramètre profil";
            this.Load += new System.EventHandler(this.Formparametre_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelprofil;
        private System.Windows.Forms.Button buttonquitter;
        private System.Windows.Forms.Button buttonsauvegarder;
        private System.Windows.Forms.Label labelnom;
        private System.Windows.Forms.TextBox textBoxnom;
        private System.Windows.Forms.Label labelprenom;
        private System.Windows.Forms.TextBox textBoxprenom;
        private System.Windows.Forms.Label labelmatieref1;
        private System.Windows.Forms.Label labelmatierefa1;
        private System.Windows.Forms.ComboBox comboBoxmatieref1;
        private System.Windows.Forms.ComboBox comboBoxmatierefa1;
        private System.Windows.Forms.ComboBox comboBoxmatieref2;
        private System.Windows.Forms.Label labelmatieref2;
        private System.Windows.Forms.Label labelmatierefa2;
        private System.Windows.Forms.ComboBox comboBoxmatierefa2;
        private System.Windows.Forms.Label erreur;
    }
}