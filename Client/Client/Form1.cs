﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;
using System.Security.Cryptography;

namespace Client
{
    public partial class Form1 : Form
    {
        //Création de la variable global email
        internal static string email;
        //Création de la variable global mdp
        internal static string mdp;
        //Création de la variable global nom
        internal static string nom;
        //Création de la variable global prenom
        internal static string prenom;

        public Form1()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Création d'un nouveau thread
            Thread newThread = new Thread(new ThreadStart(Connexion));
            //Lance le thread
            newThread.Start();
        }

        private void textBox2_OnKeyDownHandler(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //Création d'un nouveau thread
                Thread newThread = new Thread(new ThreadStart(Connexion));
                //Lance le thread
                newThread.Start();
            }
        }
        public void Connexion()
        {
            try
            {
                //Création de socketForServer de tcp client
                TcpClient socketForServer;
                //Définie la connexion sur localhost, sur le port 10
                socketForServer = new TcpClient("localhost", 10);
                //Créer networkStream
                NetworkStream networkStream = socketForServer.GetStream();
                //Créer streamReader pour lire le serveur
                System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
                //Créer streamWriter pour écrire au client
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);

                //Créer une variable str pour envoyer le message
                string str = "connexion";
                //Prépare le message à envoyer
                streamWriter.WriteLine(str);
                //Envoie le message
                streamWriter.Flush();

                //Récupère le texte dans la textBox1
                textBox1.Invoke(new MethodInvoker(delegate { email = textBox1.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(email);
                //Envoie le message
                streamWriter.Flush();

                //Récupère le texte dans la textBox2
                textBox2.Invoke(new MethodInvoker(delegate { str = textBox2.Text; }));
                //Créer une variable pour SHA256
                var sha256 = SHA256.Create();
                //Encode le message en bytes et hash le mot de passe en SHA256
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(str));
                //Convertie le les bytes du mdp en string
                mdp = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                //Prépare le message à envoyer
                streamWriter.WriteLine(mdp);
                //Envoie le message
                streamWriter.Flush();

                //Lecture du message envoyé par le serveur
                string con = streamReader.ReadLine();
                //Si le message est "ok"
                if (con == "ok")
                {
                    //Lecture du nom
                    nom = streamReader.ReadLine();
                    //Lecture du prenom
                    prenom = streamReader.ReadLine();
                    //Declare une nouvelle fenêtre
                    FormInterface formInterface = new FormInterface();
                    //Affiche une nouvelle fenêtre
                    formInterface.ShowDialog();
                }
                //Sinon on affiche une erreur
                else
                {
                    //Affiche un message d'erreur
                    label3.Invoke(new MethodInvoker(delegate { label3.Text = "Email ou mot de passe invalid"; }));
                    //Rend le message visible
                    label3.Invoke(new MethodInvoker(delegate { label3.Visible = true; }));
                }

                //Ferme la connexion
                networkStream.Close();
            }

            //Gestion d'erreur dans le programme de connexion
            catch
            {
                //Change le texte du label
                label3.Invoke(new MethodInvoker(delegate { label3.Text = "Impossible de se connecter au serveur"; }));
                //Affcihe le label d'erreur
                label3.Invoke(new MethodInvoker(delegate { label3.Visible = true; }));
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Création d'un nouveau thread
            Thread newThread = new Thread(new ThreadStart(Inscription));
            //Lance le thread
            newThread.Start();
        }

        public void Inscription()
        {
            try
            {
                //Création de socketForServer de tcp client
                TcpClient socketForServer;
                //Définie la connexion sur localhost, sur le port 10
                socketForServer = new TcpClient("localhost", 10);
                //Créer networkStream
                NetworkStream networkStream = socketForServer.GetStream();
                //Créer streamReader pour lire le serveur
                System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
                //Créer streamWriter pour écrire au client
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);

                //Créer une varibale pour le message à envoyer
                string str = "inscription";
                //Prépare le message
                streamWriter.WriteLine(str);
                //Envoie le message
                streamWriter.Flush();

                //Récupère le texte dans textBox1
                textBox1.Invoke(new MethodInvoker(delegate { email = textBox1.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(email);
                //Envoie le message
                streamWriter.Flush();

                //Récupère le texte dans textBox2 
                textBox2.Invoke(new MethodInvoker(delegate { str = textBox2.Text; }));
                //Créer une variable pour SHA256
                var sha256 = SHA256.Create();
                //Convertie le message en byte et hash le mot de passe
                var hashedBytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(str));
                //Convertie les bytes du mot de passe en string
                string mdp = BitConverter.ToString(hashedBytes).Replace("-", "").ToLower();
                //Prépare le message
                streamWriter.WriteLine(mdp);
                //Envoie le message
                streamWriter.Flush();

                //Lecture du message du serveur
                string con = streamReader.ReadLine();
                //Si le message est "ok"
                if (con == "ok")
                {
                    //Declare une nouvelle fenêtre
                    FormInscription formInscription = new FormInscription();
                    //Affiche la nouvelle fenêtre
                    formInscription.ShowDialog();
                }
                //Sinon on envoie une erreur
                else
                {
                    //Message d'erreur pour un email déjà existant
                    label3.Invoke(new MethodInvoker(delegate { label3.Text = "Email déjà existant"; }));
                    //Rend le message visible
                    label3.Invoke(new MethodInvoker(delegate { label3.Visible = true; }));
                }

                //Ferme la connexion
                networkStream.Close();
            }

            //Gestion d'erreur dans le programme de connexion
            catch
            {
                //Change le texte du label
                label3.Invoke(new MethodInvoker(delegate { label3.Text = "Impossible de se connecter au serveur"; }));
                //Affcihe le label d'erreur
                label3.Invoke(new MethodInvoker(delegate { label3.Visible = true; }));
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void buttoninscription_Click(object sender, EventArgs e)
        {
            //Création d'un nouveau thread
            Thread newThread = new Thread(new ThreadStart(Inscription));
            //Lance le thread
            newThread.Start();
        }
    }
}
