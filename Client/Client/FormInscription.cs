﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;
using System.Security.Cryptography;

namespace Client
{
    public partial class FormInscription : Form
    {
        //Variable global email
        string email = Form1.email;

        public FormInscription()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Ferme la fenêtre
            this.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //Création d'un nouveau thread
            Thread newThread = new Thread(new ThreadStart(InscriptionBis));
            //Lance le thread
            newThread.Start();
        }

        public void InscriptionBis()
        {
            //Gestion des erreurs de connexion
            try
            {
                //Création de socketForServer de tcp client
                TcpClient socketForServer;
                //Définie la connexion sur localhost, sur le port 10
                socketForServer = new TcpClient("localhost", 10);
                //Créer networkStream
                NetworkStream networkStream = socketForServer.GetStream();
                //Créer streamReader pour lire le serveur
                System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
                //Créer streamWriter pour écrire au client
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);

                //Créer une variable str pour envoyer le message
                string str = "inscriptionbis";
                //Prépare le message à envoyer
                streamWriter.WriteLine(str);
                //Envoie le message
                streamWriter.Flush();

                //Prépare le message à envoyer
                streamWriter.WriteLine(email);
                //Envoie le message
                streamWriter.Flush();

                //Créer une variable publication pour envoyer le message
                string nom = "";
                //Récupère le texte dans la textBox1
                textBoxnom.Invoke(new MethodInvoker(delegate { nom = textBoxnom.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(nom);
                //Envoie le message
                streamWriter.Flush();
                //Change la valeur de la variable global nom
                Form1.nom = nom;

                //Créer une variable publication pour envoyer le message
                string prenom = "";
                //Récupère le texte dans la textBox1
                textBoxprenom.Invoke(new MethodInvoker(delegate { prenom = textBoxprenom.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(prenom);
                //Envoie le message
                streamWriter.Flush();
                //Change la valeur dans la variable global prenom
                Form1.prenom = prenom;

                //Lecture du message envoyé par le serveur
                string con = streamReader.ReadLine();

                //Si le message est ok
                if (con == "ok")
                {
                    //Affiche un message
                    label1.Invoke(new MethodInvoker(delegate { label1.Text = "Inscription réalisé"; }));
                    //Rend le message visible
                    label1.Invoke(new MethodInvoker(delegate { label1.Visible = true; }));
                    //Affiche le message en vert
                    label1.Invoke(new MethodInvoker(delegate { label1.ForeColor = Color.Green; }));
                    //Declare une nouvelle fenêtre
                    FormInterface formInterface = new FormInterface();
                    //Affiche la nouvelle fenêtre
                    formInterface.ShowDialog();
                }
                //Sinon
                else
                {
                    //Affiche un message d'erreur
                    label1.Invoke(new MethodInvoker(delegate { label1.Text = "Impossible de réaliser votre inscription"; }));
                    //Rend le message visible
                    label1.Invoke(new MethodInvoker(delegate { label1.Visible = true; }));
                    //Affiche le message en rouge
                    label1.Invoke(new MethodInvoker(delegate { label1.ForeColor = Color.Red; }));
                }
                //Ferme la connexion
                networkStream.Close();
            }
            catch
            {
                //Affiche un message d'erreur
                label1.Invoke(new MethodInvoker(delegate { label1.Text = "Communication impossible"; }));
                //Rend le message visible
                label1.Invoke(new MethodInvoker(delegate { label1.Visible = true; }));
                //Affiche le message en rouge
                label1.Invoke(new MethodInvoker(delegate { label1.ForeColor = Color.Red; }));

            }
        }

    }
}