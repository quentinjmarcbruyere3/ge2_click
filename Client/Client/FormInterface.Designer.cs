﻿namespace Client
{
    partial class FormInterface
    {
        /// <summary>
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur Windows Form

        /// <summary>
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormInterface));
            this.boutonrecherche = new System.Windows.Forms.Button();
            this.logo = new System.Windows.Forms.PictureBox();
            this.barrederecherche = new System.Windows.Forms.TextBox();
            this.nomutilisateur = new System.Windows.Forms.Label();
            this.labelpublication = new System.Windows.Forms.Label();
            this.buttonpublication = new System.Windows.Forms.Button();
            this.buttonInserer = new System.Windows.Forms.Button();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.buttonparametre = new System.Windows.Forms.Button();
            this.labelmsgpubli = new System.Windows.Forms.Label();
            this.labelfil = new System.Windows.Forms.Label();
            this.textBoxpublication = new System.Windows.Forms.TextBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            ((System.ComponentModel.ISupportInitialize)(this.logo)).BeginInit();
            this.SuspendLayout();
            // 
            // boutonrecherche
            // 
            this.boutonrecherche.AutoSize = true;
            this.boutonrecherche.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("boutonrecherche.BackgroundImage")));
            this.boutonrecherche.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.boutonrecherche.Location = new System.Drawing.Point(634, 6);
            this.boutonrecherche.Margin = new System.Windows.Forms.Padding(2);
            this.boutonrecherche.Name = "boutonrecherche";
            this.boutonrecherche.Size = new System.Drawing.Size(60, 65);
            this.boutonrecherche.TabIndex = 0;
            this.boutonrecherche.UseVisualStyleBackColor = true;
            this.boutonrecherche.Click += new System.EventHandler(this.button1_Click);
            // 
            // logo
            // 
            this.logo.Image = ((System.Drawing.Image)(resources.GetObject("logo.Image")));
            this.logo.Location = new System.Drawing.Point(9, 0);
            this.logo.Margin = new System.Windows.Forms.Padding(2);
            this.logo.Name = "logo";
            this.logo.Size = new System.Drawing.Size(90, 98);
            this.logo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.logo.TabIndex = 1;
            this.logo.TabStop = false;
            this.logo.Click += new System.EventHandler(this.pictureBox1_Click);
            // 
            // barrederecherche
            // 
            this.barrederecherche.Location = new System.Drawing.Point(104, 30);
            this.barrederecherche.Margin = new System.Windows.Forms.Padding(2);
            this.barrederecherche.Name = "barrederecherche";
            this.barrederecherche.Size = new System.Drawing.Size(527, 20);
            this.barrederecherche.TabIndex = 2;
            // 
            // nomutilisateur
            // 
            this.nomutilisateur.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nomutilisateur.AutoSize = true;
            this.nomutilisateur.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.nomutilisateur.Location = new System.Drawing.Point(942, 30);
            this.nomutilisateur.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.nomutilisateur.Name = "nomutilisateur";
            this.nomutilisateur.Size = new System.Drawing.Size(51, 20);
            this.nomutilisateur.TabIndex = 4;
            this.nomutilisateur.Text = "label1";
            this.nomutilisateur.Click += new System.EventHandler(this.label1_Click);
            // 
            // labelpublication
            // 
            this.labelpublication.AutoSize = true;
            this.labelpublication.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelpublication.Location = new System.Drawing.Point(104, 96);
            this.labelpublication.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelpublication.Name = "labelpublication";
            this.labelpublication.Size = new System.Drawing.Size(278, 29);
            this.labelpublication.TabIndex = 5;
            this.labelpublication.Text = "Créer une publication :";
            this.labelpublication.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelpublication.Click += new System.EventHandler(this.label1_Click_1);
            // 
            // buttonpublication
            // 
            this.buttonpublication.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.buttonpublication.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonpublication.Location = new System.Drawing.Point(1177, 236);
            this.buttonpublication.Margin = new System.Windows.Forms.Padding(2);
            this.buttonpublication.Name = "buttonpublication";
            this.buttonpublication.Size = new System.Drawing.Size(150, 41);
            this.buttonpublication.TabIndex = 7;
            this.buttonpublication.Text = "Publier";
            this.buttonpublication.UseVisualStyleBackColor = true;
            this.buttonpublication.Click += new System.EventHandler(this.buttonpublication_Click);
            // 
            // buttonInserer
            // 
            this.buttonInserer.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonInserer.Location = new System.Drawing.Point(141, 236);
            this.buttonInserer.Margin = new System.Windows.Forms.Padding(2);
            this.buttonInserer.Name = "buttonInserer";
            this.buttonInserer.Size = new System.Drawing.Size(150, 41);
            this.buttonInserer.TabIndex = 8;
            this.buttonInserer.Text = "Insérer image/vidéos ...";
            this.buttonInserer.UseVisualStyleBackColor = true;
            this.buttonInserer.Click += new System.EventHandler(this.buttonInserer_Click);
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(104, 320);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1326, 444);
            this.richTextBox1.TabIndex = 10;
            this.richTextBox1.Text = "";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 10000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // buttonparametre
            // 
            this.buttonparametre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonparametre.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("buttonparametre.BackgroundImage")));
            this.buttonparametre.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.buttonparametre.Location = new System.Drawing.Point(1132, 7);
            this.buttonparametre.Margin = new System.Windows.Forms.Padding(2);
            this.buttonparametre.Name = "buttonparametre";
            this.buttonparametre.Size = new System.Drawing.Size(60, 65);
            this.buttonparametre.TabIndex = 11;
            this.buttonparametre.UseVisualStyleBackColor = true;
            this.buttonparametre.Click += new System.EventHandler(this.button1_Click_1);
            // 
            // labelmsgpubli
            // 
            this.labelmsgpubli.AutoSize = true;
            this.labelmsgpubli.Font = new System.Drawing.Font("Microsoft Sans Serif", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelmsgpubli.Location = new System.Drawing.Point(668, 244);
            this.labelmsgpubli.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelmsgpubli.Name = "labelmsgpubli";
            this.labelmsgpubli.Size = new System.Drawing.Size(134, 24);
            this.labelmsgpubli.TabIndex = 12;
            this.labelmsgpubli.Text = "Message info";
            this.labelmsgpubli.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // labelfil
            // 
            this.labelfil.AutoSize = true;
            this.labelfil.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelfil.Location = new System.Drawing.Point(646, 286);
            this.labelfil.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelfil.Name = "labelfil";
            this.labelfil.Size = new System.Drawing.Size(168, 29);
            this.labelfil.TabIndex = 13;
            this.labelfil.Text = "Fil d\'actualité";
            this.labelfil.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // textBoxpublication
            // 
            this.textBoxpublication.Location = new System.Drawing.Point(104, 137);
            this.textBoxpublication.Margin = new System.Windows.Forms.Padding(2);
            this.textBoxpublication.Multiline = true;
            this.textBoxpublication.Name = "textBoxpublication";
            this.textBoxpublication.Size = new System.Drawing.Size(1326, 84);
            this.textBoxpublication.TabIndex = 6;
            this.textBoxpublication.TextChanged += new System.EventHandler(this.textBoxpublication_TextChanged);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // FormInterface
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(1203, 716);
            this.Controls.Add(this.labelfil);
            this.Controls.Add(this.labelmsgpubli);
            this.Controls.Add(this.buttonparametre);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.buttonInserer);
            this.Controls.Add(this.buttonpublication);
            this.Controls.Add(this.textBoxpublication);
            this.Controls.Add(this.labelpublication);
            this.Controls.Add(this.nomutilisateur);
            this.Controls.Add(this.barrederecherche);
            this.Controls.Add(this.logo);
            this.Controls.Add(this.boutonrecherche);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.MaximizeBox = false;
            this.Name = "FormInterface";
            this.Text = "GE2_Click";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.FormInterface_Load);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.FormInterface_KeyDown);
            this.KeyUp += new System.Windows.Forms.KeyEventHandler(this.FormInterface_KeyDown);
            ((System.ComponentModel.ISupportInitialize)(this.logo)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button boutonrecherche;
        private System.Windows.Forms.PictureBox logo;
        private System.Windows.Forms.TextBox barrederecherche;
        private System.Windows.Forms.Label nomutilisateur;
        private System.Windows.Forms.Label labelpublication;
        private System.Windows.Forms.Button buttonpublication;
        private System.Windows.Forms.Button buttonInserer;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Button buttonparametre;
        private System.Windows.Forms.Label labelmsgpubli;
        private System.Windows.Forms.Label labelfil;
        private System.Windows.Forms.TextBox textBoxpublication;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
    }
}