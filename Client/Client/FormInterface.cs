﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Sockets;
using System.Threading;

namespace Client
{
    public partial class FormInterface : Form
    {
        //Variable global email
        string email = Form1.email;
        //Variable global nom
        string nom = Form1.nom;
        //Variable global prenom
        string prenom = Form1.prenom;
        //Varibale recept
        int recept = 0;
        //Variable lastid
        int lastid = 0;
        public FormInterface()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //Création d'un nouveau thread
            Thread newThread = new Thread(new ThreadStart(Recherche));
            //Lance le thread
            newThread.Start();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click_1(object sender, EventArgs e)
        {

        }

        private void textBoxpublication_TextChanged(object sender, EventArgs e)
        {
            
        }

        private void buttonInserer_Click(object sender, EventArgs e)
        {
            //System.Diagnostics.Process.Start(@"C:\Windows");

            

        }

        private void label1_Click_2(object sender, EventArgs e)
        {
            
        }

        private void FormInterface_Load(object sender, EventArgs e)
        {
            //Création d'un nouveau thread
            Thread newThread = new Thread(new ThreadStart(ReceptionMessage));
            //Lance le thread
            newThread.Start();
            //Change le nom d'utilisateur
            nomutilisateur.Text = prenom + " " + nom;
        }

        private void buttonpublication_Click(object sender, EventArgs e)
        {
            //Création d'un nouveau thread
            Thread newThread = new Thread(new ThreadStart(EnvoieMessage));
            //Lance le thread
            newThread.Start();
        }

        public void EnvoieMessage()
        {
            try
            {
                //Création de socketForServer de tcp client
                TcpClient socketForServer;
                //Définie la connexion sur localhost, sur le port 10
                socketForServer = new TcpClient("localhost", 10);
                //Créer networkStream
                NetworkStream networkStream = socketForServer.GetStream();
                //Créer streamReader pour lire le serveur
                System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
                //Créer streamWriter pour écrire au client
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);

                //Créer une variable str pour envoyer le message
                string str = "publication";
                //Prépare le message à envoyer
                streamWriter.WriteLine(str);
                //Envoie le message
                streamWriter.Flush();

                //Prépare le message à envoyer
                streamWriter.WriteLine(email);
                //Envoie le message
                streamWriter.Flush();

                //Créer une variable publication pour envoyer le message
                string publication = "";
                //Récupère le texte dans la textBox1
                textBoxpublication.Invoke(new MethodInvoker(delegate { publication = textBoxpublication.Text; }));
                //Prépare le message à envoyer
                streamWriter.WriteLine(publication);
                //Envoie le message
                streamWriter.Flush();

                //Lecture du message envoyé par le serveur
                string con = streamReader.ReadLine();

                if (con == "ok")
                {
                    //Affiche un message
                    labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Text = "Message publié"; }));
                    //Rend le message visible
                    labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Visible = true; }));
                    //Affiche le message en vert
                    labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.ForeColor = Color.Green; }));
                }
                else
                {
                    //Affiche un message d'erreur
                    labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Text = "Impossible de publier le message"; }));
                    //Rend le message visible
                    labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Visible = true; }));
                    //Affiche le message en rouge
                    labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.ForeColor = Color.Red; }));
                }
                //Ferme la connexion
                networkStream.Close();
            }
            catch
            {

                //Affiche un message d'erreur
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Text = "Communication impossible"; }));
                //Rend le message visible
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Visible = true; }));
                //Affiche le message en rouge
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.ForeColor = Color.Red; }));
            
            }
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            //Si la barre de recherche est vide
            if (barrederecherche.Text == "")
            {
                //Création d'un nouveau thread
                Thread newThread = new Thread(new ThreadStart(ReceptionMessage));
                //Lance le thread
                newThread.Start();
            }
        }

        public void ReceptionMessage()
        {
            //Gesttion de l'erreur si connection impossible
            try
            {
                //Rend le message invisible
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Visible = false; }));
                //Création de socketForServer de tcp client
                TcpClient socketForServer;
                //Définie la connexion sur localhost, sur le port 10
                socketForServer = new TcpClient("localhost", 10);
                //Créer networkStream
                NetworkStream networkStream = socketForServer.GetStream();
                //Créer streamReader pour lire le serveur
                System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
                //Créer streamWriter pour écrire au client
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);

                //Créer une variable str pour envoyer le message
                string str = "reception";
                //Prépare le message à envoyer
                streamWriter.WriteLine(str);
                //Envoie le message
                streamWriter.Flush();
                //Variable message
                string message = "";
                //Variable i
                int i = 0;
                //Booléen lire
                bool lire = false;

                //Si recept vaut 0 c'est que l'on a jamais lu
                if (recept == 0)
                {
                    //Clear le textBox
                    richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.Clear(); }));
                    //Boucle tant que le message fin n'est pas reçus
                    while (message != "fin")
                    {
                        //Incrémente i
                        i++;
                        //Lecture du message
                        message = streamReader.ReadLine();
                        //Si i = 1 et que message n'est pas fin
                        if (i == 1 & message != "fin")
                        {
                            //Si l'id du message est superrieur a lastid
                            if (Int32.Parse(message) > lastid)
                            {
                                //La variable lastid prend la valeur du message
                                lastid = Int32.Parse(message);
                            }
                        }
                        //Si message n'est pas fin et que i est supérieur a 1
                        if (message != "fin" & i > 1)
                        {
                            //Affiche le message
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText(message + "\r\n\r\n"); }));
                        }
                        //Si i vaut 4
                        if (i == 4)
                        {
                            //Affiche un trait
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("---------------------------------------\r\n\r\n"); }));
                            //Remet i a 0
                            i = 0;
                        }
                    }
                    //Recept vaut 1 car on à effectué la première lecture
                    recept = 1;
                }
                //Sinon
                else
                {
                    //Tant que message n'est pas fin
                    while (message != "fin")
                    {
                        //Incrémente i
                        i++;
                        //Lecture du message
                        message = streamReader.ReadLine();
                        //Si i vaut 1 et message n'est pas fin
                        if (i == 1 & message != "fin")
                        {
                            //Si l'id du message est supérieur a lastid
                            if (Int32.Parse(message) > lastid)
                            {
                                //Lire vaut true
                                lire = true;
                                //La variable lastid prend l'id du message
                                lastid = Int32.Parse(message);
                            }
                            //Sinon
                            else
                            {
                                //Lire vaut false
                                lire = false;
                            }
                        }
                        //Si lire vaut true
                        if (lire)
                        {
                            //Si message ne vaut pas fin et que i est supérieur a 1
                            if (message != "fin" & i > 1)
                            {
                                //Affiche le message
                                richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText(message + "\r\n\r\n"); }));
                            }
                            //Si i vaut 4
                            if (i == 4)
                            {
                                //Affiche une ligne
                                richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("---------------------------------------\r\n\r\n"); }));
                                //i prend la valeur 0
                                i = 0;
                            }
                        }
                        //Si i vaut 4
                        if (i == 4)
                        {
                            //i prend la valeur 0
                            i = 0;
                        }
                    }
                }

                //Ferme la connexion
                networkStream.Close();
            }
            
            //En cas d'erreur
            catch
            {
                //Affiche un message d'erreur
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Text = "Communication impossible"; }));
                //Rend le message visible
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Visible = true; }));
                //Affiche le message en rouge
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.ForeColor = Color.Red; }));
            }
        }

        public void Recherche()
        {
            //Gestion de l'erreur si connection impossible
            try
            {
                //Rend le message invisible
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Visible = false; }));
                //Création de socketForServer de tcp client
                TcpClient socketForServer;
                //Définie la connexion sur localhost, sur le port 10
                socketForServer = new TcpClient("localhost", 10);
                //Créer networkStream
                NetworkStream networkStream = socketForServer.GetStream();
                //Créer streamReader pour lire le serveur
                System.IO.StreamReader streamReader = new System.IO.StreamReader(networkStream);
                //Créer streamWriter pour écrire au client
                System.IO.StreamWriter streamWriter = new System.IO.StreamWriter(networkStream);

                //Créer une variable str pour envoyer le message
                string str = "recherche";
                //Prépare le message à envoyer
                streamWriter.WriteLine(str);
                //Envoie le message
                streamWriter.Flush();

                //Créer une variable pour la recherche
                string texterecherche = "";
                //Prend la valeur de la barre de recherche
                barrederecherche.Invoke(new MethodInvoker(delegate { texterecherche = barrederecherche.Text; }));

                //Prépare le message à envoyer
                streamWriter.WriteLine(texterecherche);
                //Envoie le message
                streamWriter.Flush();

                //Clear la texteBox
                richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.Clear(); }));

                //Variable message
                string message = "";
                //Variable i
                int i = 0;
                //Booléen lire
                bool lire = false;
                //Recept prend la valeur 0
                recept = 0;

                //Si recept vaut 0
                if (recept == 0)
                {
                    //Boucle tant que message ne vaut pas fin
                    while (message != "fin")
                    {
                        //Incrémente i
                        i++;
                        //Lecture du message
                        message = streamReader.ReadLine();
                        //Si i vaut 1 et que message est différent de fin
                        if (i == 1 & message != "fin")
                        {
                            //Si l'id du message est supérieur a lastid
                            if (Int32.Parse(message) > lastid)
                            {
                                //La variable lastid prend la valeur de l'id du message
                                lastid = Int32.Parse(message);
                            }
                        }
                        //Si message est différent de fin et i supérieur a 1
                        if (message != "fin" & i > 1)
                        {
                            //Affiche le message
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText(message + "\r\n\r\n"); }));
                        }
                        //Si i vaut 4
                        if (i == 4)
                        {
                            //Affiche un trait
                            richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("---------------------------------------\r\n\r\n"); }));
                            //i prend la valeur 0
                            i = 0;
                        }
                    }
                    //recept prend la valeur 1
                    recept = 1;
                }
                //Sinon
                else
                {
                    //Boucle tant que le message n'est pas fin
                    while (message != "fin")
                    {
                        //Incrémente i
                        i++;
                        //Lecture du message
                        message = streamReader.ReadLine();
                        //Si i vaut 1 et que message est différent de fin
                        if (i == 1 & message != "fin")
                        {
                            //Si l'id du message est supérieur a lastid
                            if (Int32.Parse(message) > lastid)
                            {
                                //Lire vaut true
                                lire = true;
                                //lastid prend la valeur de l'id du message
                                lastid = Int32.Parse(message);
                            }
                            //Sinon
                            else
                            {
                                //Lire vaut false
                                lire = false;
                            }
                        }
                        //Si lire vaut true
                        if (lire)
                        {
                            //Si message est différent de fin et i supérieur a 1
                            if (message != "fin" & i > 1)
                            {
                                //Affiche le message
                                richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText(message + "\r\n\r\n"); }));
                            }
                            //Si i vaut 4
                            if (i == 4)
                            {
                                //Affiche un trait
                                richTextBox1.Invoke(new MethodInvoker(delegate { richTextBox1.AppendText("---------------------------------------\r\n\r\n"); }));
                                //i prend la valeur 0
                                i = 0;
                            }
                        }
                        //Si i vaut 4
                        if (i == 4)
                        {
                            //i prend la valeur 0
                            i = 0;
                        }
                    }
                }

                //Ferme la connexion
                networkStream.Close();
                //recept prend la valeur 0
                recept = 0;
            }

            //Si problemen de connexion affiche un message d'erreur
            catch
            {
                //Affiche un message d'erreur
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Text = "Communication impossible"; }));
                //Rend le message visible
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.Visible = true; }));
                //Affiche le message en rouge
                labelmsgpubli.Invoke(new MethodInvoker(delegate { labelmsgpubli.ForeColor = Color.Red; }));
            }
        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            //Déclare une nouvelle fenêtre
            Formparametre formparametre = new Formparametre();
            //Affiche la fenêtre
            formparametre.ShowDialog();
        }

        private void FormInterface_KeyDown(object sender, KeyEventArgs e)
        {
            //Si la touche espace est pressé
            if (e.KeyCode == Keys.Escape)
            {
                //Déclare une nouvelle fenêtre
                Formparametre formparametre = new Formparametre();
                //Affiche la fenêtre
                formparametre.ShowDialog();
            }
        }
    }
}
