﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testpGsql
{
    public class Contact
    {
        public int id { get; set; }
        public string nom { get; set; }
        public string tel { get; set; }

        // Constructeur
        public Contact()
        {
        }
    }
}
