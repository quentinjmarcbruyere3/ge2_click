﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Référencer packages Npgsql & Mono.security
using Npgsql;
using NpgsqlTypes;


// Créer base de donnée pgSql:
// base: maBase
// table: repertoire
// colonnes: id, nom, telephone

namespace testpGsql
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }


        // paramètres connexion à bdd
        string Conx = "Server=localhost;Port=5432;Database=maBase;User Id=postgres;Password=;";
        NpgsqlConnection Cnx = null;

        //NpgsqlCommand MyCmd = null;
        

        public void AjoutContact(Contact contact) // Méthode d'écriture dans la base
        {
            Cnx = new NpgsqlConnection(Conx);
            string insert = "INSERT INTO \"repertoire\"(nom,telephone) values(:nom,:tel)";
                                    
            Cnx.Open(); // ouverture connexion
            NpgsqlCommand Cmd = new NpgsqlCommand(insert, Cnx); // requête sql

            
            Cmd.Parameters.Add(new NpgsqlParameter("nom", NpgsqlDbType.Varchar)).Value = contact.nom;
            Cmd.Parameters.Add(new NpgsqlParameter("tel", NpgsqlDbType.Varchar)).Value = contact.tel;
                                    
            Cmd.ExecuteNonQuery(); //Exécution de la requête
            Cnx.Close();
        }


        public void LectureContacts() // Méthode de lecture de la base
        {
            DataTable MyData = new DataTable();
            NpgsqlDataAdapter da;
            Cnx = new NpgsqlConnection(Conx);
            string select = "SELECT * FROM \"repertoire\"";

            Cnx.Open();

            NpgsqlCommand Cmd = new NpgsqlCommand(select, Cnx);
            da = new NpgsqlDataAdapter(Cmd);

            da.Fill(MyData);
            dataGridView1.DataSource = MyData;

            Cnx.Close();
        }


        private void button_W_Click(object sender, EventArgs e)
        {
            Contact contact = new Contact();

            contact.nom = textBox_nom.Text;
            contact.tel = textBox_tel.Text;
            AjoutContact(contact);

        }


        private void button_R_Click(object sender, EventArgs e)
        {
            LectureContacts();
        }



               


    }
}
