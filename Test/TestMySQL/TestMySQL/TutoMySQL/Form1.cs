﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// Librairie MySQL ajoutée dans les références.
using MySql.Data.MySqlClient;

//Créer une bdd MySQL et une table (id, nam, tel) ac auto incrémentation des entrées
//
// nom de bdd: mydb
// nom de table: repertoire

namespace TestMySQL
{
    public partial class Form1 : Form
    {
        // paramètres connection mySQL
        private MySqlConnection connection;
        string connectionString = "SERVER=127.0.0.1; DATABASE=mydb; UID=root; PASSWORD=";

        public Form1()
        {
            InitializeComponent();
        }


        // ECRITURE ds la base
        public void button_w_Click(object sender, EventArgs e)
        {

        // Création d'un contact à ajouter grâce à la classe Contact
        Contact contact = new Contact();
            contact.Name = textBox_name.Text;
            contact.Tel = textBox_tel.Text;

            // Connection à Mysql
            connection = new MySqlConnection(connectionString);
            try
            {
                // Ouverture de la connexion SQL
                connection.Open();

                // Création d'une requête SQL (écriture)
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "INSERT INTO repertoire (name, tel) VALUES (@name, @tel)"; 

                // paramètres à transmettre
                cmd.Parameters.AddWithValue("@name", contact.Name);
                cmd.Parameters.AddWithValue("@tel", contact.Tel);

                // Exécution de la commande SQL
                cmd.ExecuteNonQuery();

                // Fermeture de la connexion
                connection.Close();
            }
            catch
            {
                // Gestion des erreurs :
                // Possibilité de créer un Logger pour les exceptions SQL reçus
                // Possibilité de créer une méthode avec un booléan en retour pour savoir si le contact à été ajouté correctement.
            }
            
        }



        // LECTURE ds la base
        private void button_r_Click(object sender, EventArgs e)
        {
            // Création d'un contact à lire
            Contact contact = new Contact();

            // Connection Mysql
            connection = new MySqlConnection(connectionString);
            try
            {
                // Ouverture de la connexion SQL
                connection.Open();

                // Création d'une requête SQL
                MySqlCommand cmd = connection.CreateCommand();
                cmd.CommandText = "SELECT id, name, tel FROM repertoire";


                // création d'un reader et lecture de table dans comboBox
                MySqlDataReader rdr = cmd.ExecuteReader();

                //comboBox_r.Items.Clear();
                while (rdr.Read())
                {
                    comboBox_r.Items.Add(rdr["Id"] + " - " + rdr["Name"] + " - " + rdr["Tel"]);
                    
                }
                rdr.Close();


                // lecture table dans dataGridView
                cmd.CommandText = "SELECT * FROM repertoire";
                MySqlDataAdapter adap = new MySqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                adap.Fill(ds);
                dataGridView_r.DataSource = ds.Tables[0].DefaultView;

                
                // Fermeture de la connexion
                connection.Close();
            }
            catch
            {
                // Gestion des erreurs :
                // Possibilité de créer un Logger pour les exceptions SQL reçus
                // Possibilité de créer une méthode avec un booléan en retour pour savoir si le contact à été ajouté correctement.
            }



        }

        
    }
}

