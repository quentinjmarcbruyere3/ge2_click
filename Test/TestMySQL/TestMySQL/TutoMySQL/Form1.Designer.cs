﻿namespace TestMySQL
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.button_w = new System.Windows.Forms.Button();
            this.textBox_name = new System.Windows.Forms.TextBox();
            this.textBox_tel = new System.Windows.Forms.TextBox();
            this.label_name = new System.Windows.Forms.Label();
            this.label_tel = new System.Windows.Forms.Label();
            this.button_r = new System.Windows.Forms.Button();
            this.comboBox_r = new System.Windows.Forms.ComboBox();
            this.dataGridView_r = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_r)).BeginInit();
            this.SuspendLayout();
            // 
            // button_w
            // 
            this.button_w.Location = new System.Drawing.Point(25, 32);
            this.button_w.Name = "button_w";
            this.button_w.Size = new System.Drawing.Size(75, 23);
            this.button_w.TabIndex = 0;
            this.button_w.Text = "write";
            this.button_w.UseVisualStyleBackColor = true;
            this.button_w.Click += new System.EventHandler(this.button_w_Click);
            // 
            // textBox_name
            // 
            this.textBox_name.Location = new System.Drawing.Point(183, 35);
            this.textBox_name.Name = "textBox_name";
            this.textBox_name.Size = new System.Drawing.Size(100, 20);
            this.textBox_name.TabIndex = 2;
            this.textBox_name.Text = "bb";
            // 
            // textBox_tel
            // 
            this.textBox_tel.Location = new System.Drawing.Point(327, 35);
            this.textBox_tel.Name = "textBox_tel";
            this.textBox_tel.Size = new System.Drawing.Size(100, 20);
            this.textBox_tel.TabIndex = 3;
            this.textBox_tel.Text = "0477";
            // 
            // label_name
            // 
            this.label_name.AutoSize = true;
            this.label_name.Location = new System.Drawing.Point(183, 12);
            this.label_name.Name = "label_name";
            this.label_name.Size = new System.Drawing.Size(33, 13);
            this.label_name.TabIndex = 5;
            this.label_name.Text = "name";
            // 
            // label_tel
            // 
            this.label_tel.AutoSize = true;
            this.label_tel.Location = new System.Drawing.Point(327, 12);
            this.label_tel.Name = "label_tel";
            this.label_tel.Size = new System.Drawing.Size(18, 13);
            this.label_tel.TabIndex = 6;
            this.label_tel.Text = "tel";
            // 
            // button_r
            // 
            this.button_r.Location = new System.Drawing.Point(25, 109);
            this.button_r.Name = "button_r";
            this.button_r.Size = new System.Drawing.Size(75, 23);
            this.button_r.TabIndex = 7;
            this.button_r.Text = "read";
            this.button_r.UseVisualStyleBackColor = true;
            this.button_r.Click += new System.EventHandler(this.button_r_Click);
            // 
            // comboBox_r
            // 
            this.comboBox_r.FormattingEnabled = true;
            this.comboBox_r.Location = new System.Drawing.Point(183, 111);
            this.comboBox_r.Name = "comboBox_r";
            this.comboBox_r.Size = new System.Drawing.Size(121, 21);
            this.comboBox_r.TabIndex = 9;
            // 
            // dataGridView_r
            // 
            this.dataGridView_r.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_r.Location = new System.Drawing.Point(450, 75);
            this.dataGridView_r.Name = "dataGridView_r";
            this.dataGridView_r.Size = new System.Drawing.Size(406, 261);
            this.dataGridView_r.TabIndex = 10;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 348);
            this.Controls.Add(this.dataGridView_r);
            this.Controls.Add(this.comboBox_r);
            this.Controls.Add(this.button_r);
            this.Controls.Add(this.label_tel);
            this.Controls.Add(this.label_name);
            this.Controls.Add(this.textBox_tel);
            this.Controls.Add(this.textBox_name);
            this.Controls.Add(this.button_w);
            this.Name = "Form1";
            this.Text = "MySQL_R/W";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_r)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button_w;
        private System.Windows.Forms.TextBox textBox_name;
        private System.Windows.Forms.TextBox textBox_tel;
        private System.Windows.Forms.Label label_name;
        private System.Windows.Forms.Label label_tel;
        private System.Windows.Forms.Button button_r;
        private System.Windows.Forms.ComboBox comboBox_r;
        private System.Windows.Forms.DataGridView dataGridView_r;
    }
}